# Copyright 2021 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist=2 has_bin=true multibuild=false ]
require meson
require test-dbus-daemon

SUMMARY="Development and debugging tools for GStreamer"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="
    LGPL-2.1
    gui? ( GPL-3 )
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gui [[ description = [ Build the gst-debug-viewer GUI tool ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
    build+run:
        core/json-glib
        dev-libs/glib:2[>=2.56.0][gobject-introspection(+)?]
        media-libs/gstreamer:1.0[>=$(ever range 1-2)]
        media-plugins/gst-plugins-base:1.0[>=$(ever range 1-2)]
        x11-libs/cairo
        gui? ( x11-libs/gtk+:3 )
"

# Fails all validate_* tests
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dnls=enabled
    -Dvalidate=enabled

    -Dcairo=enabled

    # Needs hotdoc
    -Ddoc=disabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
    'gui debug_viewer'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

